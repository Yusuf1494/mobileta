package jl;

import java.io.Serializable;

/**
 *
 * @author Wakka
 */
public class About implements Serializable {

	private static final long serialVersionUID = 1L;
        
        private String about;
        
        public About(){
        }
    
        public About(String about){
            this.about = about;
        }
        
}
