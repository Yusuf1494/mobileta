/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function onLoad(){
	initMap();
}

function initMap() {
	map = new google.maps.Map(document.getElementById("map"), {
		center: {lat: -6.9066029, lng: 107.5980013},
		zoom: 12,
		streetViewControl: false,
		mapTypeControl: false,
		mapTypeId: 'roadmap'
	});
	console.log('here!');
	map.setOptions({minZoom: 12, maxZoom: 20});
}